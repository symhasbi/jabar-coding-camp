//Soal 1
let panjang= 5
let lebar = 3

function luasPersegipanjang (panjang,lebar){
    return panjang * lebar
}
console.log(luasPersegipanjang(panjang,lebar))

function kelilingPersegipanjang(panjang,lebar) {
    return (panjang + lebar ) * 2
}
console.log(kelilingPersegipanjang (panjang,lebar))

//Soal 2

const newFunction = (firstName, lastName)=>{
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName() 


//soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }
  const {firstName,lastName,address,hobby}=newObject
  console.log(firstName,lastName,address, hobby)

//soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [west,east]
//Driver Code
console.log(combined)

//Soal 5 
const planet = "earth" 
const view = "glass" 
// var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
console.log(`Lorem ${view} dolor sit amet consectetur adipiscing elit ${planet} `)