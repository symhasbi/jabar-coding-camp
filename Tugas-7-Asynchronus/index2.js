//Jawaban Soal 2

var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

function baca(sisaWaktu, books, i) {
    readBooksPromise(sisaWaktu, books[i], function(time) {
        const nextBook = i + 1;
        if (nextBook < books.length) {
            baca(time, books, nextBook);
        }
    });
}

baca(10000, books, 0);