// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var hewanUrut = []
for (let i = 0; i < daftarHewan.length; i++) {
    var hewan = daftarHewan[i] // "2. Komodo"
    // ambil karakter pertama dari list hewan
    var indexHewan = hewan.substring(0, 1)
    // ubah karakter pertama jadi integer
    var nomer=Number(indexHewan)
    // masukan hewan ke hewanUrut sesuai integer yg diconvert // hint hewanUrut[index]
    hewanUrut[nomer-1]=hewan
}

for (let i = 0; i < hewanUrut.length; i++) {
  console.log(hewanUrut[i])
    
}

// soal 2
function introduce(data) {
    return `Nama saya ${data.name}, umur saya ${data.age} tahun, alamat saya di ${data.address} dan saya punya hobby yaitu ${data.hobby}
    `
}
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }

var perkenalan = introduce(data)
console.log(perkenalan)

// soal 3
var vokal = ['a', 'i', 'u', 'e', 'o']
function hitung_huruf_vokal(nama) {
    var jumlah = 0
    for (let i = 0; i < nama.length; i++) {
        if (vokal.includes(nama.charAt(i).toLowerCase())) {
            jumlah++
        }
    }
    return jumlah
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)

// soal 4
function hitung(angka) {
    return (angka * 2) - 2
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8