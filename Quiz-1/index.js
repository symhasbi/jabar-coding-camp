//soal 1 Function Penghasil Tanggal Hari Esok

function next_date(tanggal,bulan){
   var besok= tanggal+1
   if (bulan==1 && tanggal==31) {
    besok=1
    }
    else if (bulan==2 && tanggal==28) {
       besok=1
    }
    else if (bulan==3 && tanggal==31) {
       besok=1
    }
    else if (bulan==4 && tanggal==30) {
       besok=1
    }
    else if (bulan==5 && tanggal==31) {
       besok=1
    }
    else if (bulan==6 && tanggal==30) {
       besok=1
    }
    else if (bulan==7 && tanggal==31) {
       besok=1
    }
    else if (bulan==8 && tanggal==31) {
       besok=1
    }
    else if (bulan==9 && tanggal==30) {
       besok=1
    }
    else if (bulan==10 && tanggal==31) {
       besok=1
    }
    else if (bulan==11 && tanggal==30) {
       besok=1
    }
    else if (bulan==12 && tanggal==31) {
       besok=1
    }
    
    return besok
}


function full_date(tanggal,bulan,tahun){
// 1 panggil function next date dengan argumen tanggal dan bulan

var besok = next_date(tanggal,bulan)

//2 jika hasil function==1 maka bulan+1 
if (besok==1) {
    var bulanBesok = bulan+1
}
else {var bulanBesok=bulan}
//3 jika bulan+1 == 13 maka tahun+1

if (bulanBesok==13) {
    var tahunBesok=tahun+1
    bulanBesok=1
}
else{var tahunBesok=tahun}

switch (bulanBesok) {
    case 1:
        bulanBesok= "Januari"
        break;
    case 2:
        bulanBesok= "Februari"
        break;
    case 3:
        bulanBesok= "Maret"
        break;
    case 4:
        bulanBesok= "April"
        break;
    case 5:
        bulanBesok= "Mei"
        break;
    case 6:
        bulanBesok= "Juni"
        break;
    case 7:
        bulanBesok= "Juli"
        break;
    case 8:
        bulanBesok= "Agustus"
        break;
    case 9:
        bulanBesok= "September"
        break;
    case 10:
        bulanBesok= "Oktober"
        break;
    case 11:
        bulanBesok= "November"
        break;
    case 12:
        bulanBesok= "Desember"
        break;
}

return (besok + " " + bulanBesok +" " + tahunBesok)
}

console.log(full_date(31,12,2021))

//Soal 2 Function Penghitung Jumlah Kata

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = " Saya Iqbal"
var kalimat_3 = " Saya Muhammad Iqbal Mubarok "

function jumlah_kata(s){
    return s.trim().split(' ').length
}
console.log(jumlah_kata(kalimat_1))
console.log(jumlah_kata(kalimat_2))
console.log(jumlah_kata(kalimat_3))